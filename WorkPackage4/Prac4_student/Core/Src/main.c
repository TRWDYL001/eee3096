/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include "stm32f0xx.h"
#include <lcd_stm32f0.c>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
// TODO: Add values for below variables
#define NS 128     // Number of samples in LUT
#define TIM2CLK 8000000   // STM Clock frequency
#define F_SIGNAL 50 // Frequency of output analog signal
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
DMA_HandleTypeDef hdma_tim2_ch1;

/* USER CODE BEGIN PV */
// TODO: Add code for global variables, including LUTs


char sine[] = "Sine";
char saw[] = "Saw";
char triangle[] = "Triangle";
uint32_t currentWaveform = 0;
uint32_t prev_millis = 0;
uint32_t delay_t = 500; // Initialise delay to 500ms
uint32_t sinusoid_lut[NS] = {511.500000,
		  536.795578,
		  562.029253,
		  587.139275,
		  612.064194,
		  636.743017,
		  661.115348,
		  685.121546,
		  708.702863,
		  731.801591,
		  754.361204,
		  776.326496,
		  797.643712,
		  818.260687,
		  838.126967,
		  857.193937,
		  875.414935,
		  892.745372,
		  909.142838,
		  924.567206,
		  938.980728,
		  952.348134,
		  964.636711,
		  975.816386,
		  985.859801,
		  994.742378,
		  1002.442380,
		  1008.940963,
		  1014.222226,
		  1018.273243,
		  1021.084100,
		  1022.647921,
		  1022.960876,
		  1022.022201,
		  1019.834193,
		  1016.402205,
		  1011.734638,
		  1005.842912,
		  998.741447,
		  990.447620,
		  980.981728,
		  970.366935,
		  958.629218,
		  945.797301,
		  931.902586,
		  916.979075,
		  901.063289,
		  884.194176,
		  866.413019,
		  847.763329,
		  828.290747,
		  808.042925,
		  787.069413,
		  765.421536,
		  743.152270,
		  720.316113,
		  696.968947,
		  673.167908,
		  648.971241,
		  624.438158,
		  599.628697,
		  574.603570,
		  549.424018,
		  524.151660,
		  498.848340,
		  473.575982,
		  448.396430,
		  423.371303,
		  398.561842,
		  374.028759,
		  349.832092,
		  326.031053,
		  302.683887,
		  279.847730,
		  257.578464,
		  235.930587,
		  214.957075,
		  194.709253,
		  175.236671,
		  156.586981,
		  138.805824,
		  121.936711,
		  106.020925,
		  91.097414,
		  77.202699,
		  64.370782,
		  52.633065,
		  42.018272,
		  32.552380,
		  24.258553,
		  17.157088,
		  11.265362,
		  6.597795,
		  3.165807,
		  0.977799,
		  0.039124,
		  0.352079,
		  1.915900,
		  4.726757,
		  8.777774,
		  14.059037,
		  20.557620,
		  28.257622,
		  37.140199,
		  47.183614,
		  58.363289,
		  70.651866,
		  84.019272,
		  98.432794,
		  113.857162,
		  130.254628,
		  147.585065,
		  165.806063,
		  184.873033,
		  204.739313,
		  225.356288,
		  246.673504,
		  268.638796,
		  291.198409,
		  314.297137,
		  337.878454,
		  361.884652,
		  386.256983,
		  410.935806,
		  435.860725,
		  460.970747,
		  486.204422,
		  511.500000};

uint32_t sawtooth_lut[NS] = {0.000000,
		8.055118,
		16.110236,
		24.165354,
		32.220472,
		40.275591,
		48.330709,
		56.385827,
		64.440945,
		72.496063,
		80.551181,
		88.606299,
		96.661417,
		104.716535,
		112.771654,
		120.826772,
		128.881890,
		136.937008,
		144.992126,
		153.047244,
		161.102362,
		169.157480,
		177.212598,
		185.267717,
		193.322835,
		201.377953,
		209.433071,
		217.488189,
		225.543307,
		233.598425,
		241.653543,
		249.708661,
		257.763780,
		265.818898,
		273.874016,
		281.929134,
		289.984252,
		298.039370,
		306.094488,
		314.149606,
		322.204724,
		330.259843,
		338.314961,
		346.370079,
		354.425197,
		362.480315,
		370.535433,
		378.590551,
		386.645669,
		394.700787,
		402.755906,
		410.811024,
		418.866142,
		426.921260,
		434.976378,
		443.031496,
		451.086614,
		459.141732,
		467.196850,
		475.251969,
		483.307087,
		491.362205,
		499.417323,
		507.472441,
		515.527559,
		523.582677,
		531.637795,
		539.692913,
		547.748031,
		555.803150,
		563.858268,
		571.913386,
		579.968504,
		588.023622,
		596.078740,
		604.133858,
		612.188976,
		620.244094,
		628.299213,
		636.354331,
		644.409449,
		652.464567,
		660.519685,
		668.574803,
		676.629921,
		684.685039,
		692.740157,
		700.795276,
		708.850394,
		716.905512,
		724.960630,
		733.015748,
		741.070866,
		749.125984,
		757.181102,
		765.236220,
		773.291339,
		781.346457,
		789.401575,
		797.456693,
		805.511811,
		813.566929,
		821.622047,
		829.677165,
		837.732283,
		845.787402,
		853.842520,
		861.897638,
		869.952756,
		878.007874,
		886.062992,
		894.118110,
		902.173228,
		910.228346,
		918.283465,
		926.338583,
		934.393701,
		942.448819,
		950.503937,
		958.559055,
		966.614173,
		974.669291,
		982.724409,
		990.779528,
		998.834646,
		1006.889764,
		1014.944882,
		1023.000000,};

uint32_t triangular_lut[NS] = {0.000000,
		16.238095,
		32.476190,
		48.714286,
		64.952381,
		81.190476,
		97.428571,
		113.666667,
		129.904762,
		146.142857,
		162.380952,
		178.619048,
		194.857143,
		211.095238,
		227.333333,
		243.571429,
		259.809524,
		276.047619,
		292.285714,
		308.523810,
		324.761905,
		341.000000,
		357.238095,
		373.476190,
		389.714286,
		405.952381,
		422.190476,
		438.428571,
		454.666667,
		470.904762,
		487.142857,
		503.380952,
		519.619048,
		535.857143,
		552.095238,
		568.333333,
		584.571429,
		600.809524,
		617.047619,
		633.285714,
		649.523810,
		665.761905,
		682.000000,
		698.238095,
		714.476190,
		730.714286,
		746.952381,
		763.190476,
		779.428571,
		795.666667,
		811.904762,
		828.142857,
		844.380952,
		860.619048,
		876.857143,
		893.095238,
		909.333333,
		925.571429,
		941.809524,
		958.047619,
		974.285714,
		990.523810,
		1006.761905,
		1023.000000,
		1023.000000,
		1006.761905,
		990.523810,
		974.285714,
		958.047619,
		941.809524,
		925.571429,
		909.333333,
		893.095238,
		876.857143,
		860.619048,
		844.380952,
		828.142857,
		811.904762,
		795.666667,
		779.428571,
		763.190476,
		746.952381,
		730.714286,
		714.476190,
		698.238095,
		682.000000,
		665.761905,
		649.523810,
		633.285714,
		617.047619,
		600.809524,
		584.571429,
		568.333333,
		552.095238,
		535.857143,
		519.619048,
		503.380952,
		487.142857,
		470.904762,
		454.666667,
		438.428571,
		422.190476,
		405.952381,
		389.714286,
		373.476190,
		357.238095,
		341.000000,
		324.761905,
		308.523810,
		292.285714,
		276.047619,
		259.809524,
		243.571429,
		227.333333,
		211.095238,
		194.857143,
		178.619048,
		162.380952,
		146.142857,
		129.904762,
		113.666667,
		97.428571,
		81.190476,
		64.952381,
		48.714286,
		32.476190,
		16.238095,
		0.000000};

// TODO: Equation to calculate TIM2_Ticks
uint32_t TIM2_Ticks = TIM2CLK / (F_SIGNAL * NS); // How often to write new LUT value

uint32_t DestAddress = (uint32_t) &(TIM3->CCR3); // Write LUT TO TIM3->CCR3 to modify PWM duty cycle

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);

/* USER CODE BEGIN PFP */
void EXTI0_1_IRQHandler(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  init_LCD();
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();

  /* USER CODE BEGIN 2 */
  // TODO: Start TIM3 in PWM mode on channel 3
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3);


  // TODO: Start TIM2 in Output Compare (OC) mode on channel 1.

  HAL_TIM_OC_Start(&htim2, TIM_CHANNEL_1);

  // TODO: Start DMA in IT mode on TIM2->CH1; Source is LUT and Dest is TIM3->CCR3; start with Sine LUT
  // Start DMA in IT mode

  HAL_DMA_Start_IT(&hdma_tim2_ch1, (uint32_t)sinusoid_lut, (uint32_t)&TIM3->CCR3, NS);

  //HAL_TIM_OC_Start_DMA(&htim2, TIM_CHANNEL_1);

  // TODO: Write current waveform to LCD ("Sine")
  delay(3000);
  lcd_command(CLEAR);
  lcd_putstring(sine);

  // TODO: Enable DMA (start transfer from LUT to CCR)
  __HAL_TIM_ENABLE_DMA(&htim2, TIM_DMA_CC1);

				  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  LL_FLASH_SetLatency(LL_FLASH_LATENCY_0);
  while(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_0)
  {
  }
  LL_RCC_HSI_Enable();

   /* Wait till HSI is ready */
  while(LL_RCC_HSI_IsReady() != 1)
  {

  }
  LL_RCC_HSI_SetCalibTrimming(16);
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_HSI);

   /* Wait till System clock is ready */
  while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_HSI)
  {

  }
  LL_SetSystemCoreClock(8000000);

   /* Update the time base */
  if (HAL_InitTick (TICK_INT_PRIORITY) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = TIM2_Ticks - 1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1023;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel4_5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel4_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel4_5_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  LL_EXTI_InitTypeDef EXTI_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOF);
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);

  /**/
  LL_SYSCFG_SetEXTISource(LL_SYSCFG_EXTI_PORTA, LL_SYSCFG_EXTI_LINE0);

  /**/
  LL_GPIO_SetPinPull(Button0_GPIO_Port, Button0_Pin, LL_GPIO_PULL_UP);

  /**/
  LL_GPIO_SetPinMode(Button0_GPIO_Port, Button0_Pin, LL_GPIO_MODE_INPUT);

  /**/
  EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_0;
  EXTI_InitStruct.LineCommand = ENABLE;
  EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
  EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_RISING;
  LL_EXTI_Init(&EXTI_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
  HAL_NVIC_SetPriority(EXTI0_1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */
void EXTI0_1_IRQHandler(void)
{

	if (HAL_GPIO_ReadPin(Button0_GPIO_Port, Button0_Pin) == GPIO_PIN_SET ){
	    if (HAL_GetTick() - prev_millis > delay_t) {
	        // Debounce the button press
	        prev_millis = HAL_GetTick();

	        // Disable DMA transfer for TIM2_CH1
	        __HAL_TIM_DISABLE_DMA(&htim2, TIM_DMA_CC1);
	        HAL_DMA_Abort_IT(&hdma_tim2_ch1);

	        // Change the source address based on the current waveform
	        switch (currentWaveform) {
	            case 0:
	                // Switch to Sawtooth LUT
	                HAL_DMA_Start_IT(&hdma_tim2_ch1, (uint32_t)sawtooth_lut, (uint32_t)&TIM3->CCR3, NS);
	                currentWaveform = 1;
	                lcd_command(CLEAR);
	                lcd_putstring(saw);
	                break;
	            case 1:
	                // Switch to Triangular LUT
	                HAL_DMA_Start_IT(&hdma_tim2_ch1, (uint32_t)triangular_lut, (uint32_t)&TIM3->CCR3, NS);
	                currentWaveform = 2;
	                lcd_command(CLEAR);
	                lcd_putstring(triangle);
	                break;
	            case 2:
	                // Switch back to Sine LUT
	                HAL_DMA_Start_IT(&hdma_tim2_ch1, (uint32_t)sinusoid_lut, (uint32_t)&TIM3->CCR3, NS);
	                currentWaveform = 0;
	                lcd_command(CLEAR);
	                lcd_putstring(sine);
	                break;
	        }
	        // Re-enable DMA transfer for TIM2_CH1
	                __HAL_TIM_ENABLE_DMA(&htim2, TIM_DMA_CC1);
	    }


	// TODO: Disable DMA transfer and abort IT, then start DMA in IT mode with new LUT and re-enable transfer
	// HINT: Consider using C's "switch" function to handle LUT changes


	}
	HAL_GPIO_EXTI_IRQHandler(Button0_Pin); // Clear interrupt flags
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
